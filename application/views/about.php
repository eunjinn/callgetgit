<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/about.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/main/index">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- About_1 -->
	<section id="about_1">
		<div class="cover"></div>
		<div class="container">
			<div class="row">
				<div class="section-header">
					<h3>가지고 싶은 상품이 있으셨나요?</h3>
					<p>이제, 콜겟으로 가지고 싶은 상품을<br>무료로 가져가세요.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- About_1_END -->

	<!-- About_2 -->
	<section id="about_2">
		<div class="container">
			<div class="row">
				<div class="section-header">
					<h3>콜겟이 뭐죠?</h3>
					<p>브랜드사의 광고를 시청하고 전화 상담시 콜겟에서 준비한 상품을 무료로 받아볼 수 있는 서비스 입니다.</p>
				</div>
				<div class="symbol col-xs-12">
					<div>
						<img src="/assets/images/callget.png">
					</div>
				</div>
				<div class="step_area">
					<div class="steps">
						<p>1. 서비스(광고상품) 제공</p>
						<p>브랜드사는 콜겟 플랫폼에 서비스(광고상품)를 제공합니다.</p>
					</div>
					<div class="steps">
						<p>2. 광고시청 및 전화상담</p>
						<p>콜겟 이용자는 콜겟 플랫폼에서 서비스(광고상품)에 대한 광고시청 및 전화상담을 합니다.</p>
					</div>
					<div class="steps">
						<p>3. 사은품 제공</p>
						<p>전화상담이 완료되면 콜겟 플랫폼에서 콜겟 이용자에게 사은품을 제공해드립니다.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- About_2_END -->

	<!-- About_3 -->
	<section id="about_3">
		<div class="video-container">
			<iframe width="100%" src="https://www.youtube.com/embed/XHBJ1pcakVQ?ps=blogger&controls=0&iv_load_policy=3" frameborder="0" allowfullscreen="true"></iframe>
		</div>
	</section>
	<!-- About_3_END -->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Javascript files -->
	<script src="/assets/js/global.js"></script>
</body>
</html>
