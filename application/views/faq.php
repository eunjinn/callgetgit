<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/faq.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/main/index">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- FAQ_1 -->
	<section id="faq_1" class="faq mt50">
		<div class="container">
			<div class="row">
				<div class="section-header">
					<h3>콜겟 이용방법</h3>
					<div class="explain">
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_1.png">
							<p>1. 사은품 선택</p>
							<p>콜겟 메인페이지에서 원하는 사은품을 선택.</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_2.png">
							<p>2. 사은품 확인</p>
							<p>상품페이지에서 사은품에 대한 정보확인.</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_3.png">
							<p>3. 사은품 담기</p>
							<p>사은품이 맘에 들면 Get버튼 누르기.</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_4.png">
							<p>4. 광고시청</p>
							<p>브랜드사에서 준비한 광고시청 후 Call버튼 누르기.</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_5.png">
							<p>5. 전화상담</p>
							<p>브랜드사 상담원과 전화상담.</p>
						</div>
						<div class="col-xs-6 col-sm-4 col-md-2 ex">
							<img src="/assets/images/faq/sym_6.png">
							<p>6. 사은품 수령</p>
							<p>집으로 배송되는 사은품 수령.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FAQ_1_END -->

	<!-- FAQ_2 -->
	<section id="faq_2" class="faq">
		<div class="video-container">
			<iframe width="100%" src="https://www.youtube.com/embed/XHBJ1pcakVQ?ps=blogger&controls=0&iv_load_policy=3" frameborder="0" allowfullscreen="true"></iframe>
		</div>
	</section>
	<!-- FAQ_2_END -->

	<!-- FAQ_3 -->
	<section id="faq_3" class="faq">
		<div class="container">
			<div class="row">
				<div class="section-header">
					<h3>FAQ</h3>
				</div>
			</div>
			<div class="row">
				<div class="panel-group" id="accordion">
					<? for($i=0; $i < count($list); $i++) {?>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title panel-title-adjust">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>">
										Q) <?=$list[$i]->faq_question?>
									</a>
								</h4>
							</div>
							<div id="collapse<?=$i?>" class="panel-collapse collapse">
								<div class="panel-body">
									A) <?=nl2br($list[$i]->faq_answer)?>
								</div>
							</div>
						</div>

					<?}?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- FAQ_3_END -->

<!-- Scroll to top -->
<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
	<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
</div>
<!-- Scroll to top_END-->

<!-- Javascript files -->
<script src="/assets/js/global.js"></script>
</body>
</html>
