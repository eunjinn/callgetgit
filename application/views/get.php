<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/get.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/main/index">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Get_Banner -->
	<section id="get_banner" class="main_banner">
		<div class="get_banner_head">
			<div class="brand">
				<img src="/assets/uploads/<?=$data->insure_logo?>">
				<p><?=$data->insure_company?></p>
				<p>D-<?=$data->dates?></p>
			</div>
		</div>
		<div class="get_count">
			<p>현재까지<b><?=$data->product_get?></b>개</p>
			<p>Get 하셨습니다.</p>
		</div>
		<div id="myCarousel" class="carousel slide">
			<div class="carousel-inner">
				<? $i = 0;
					foreach($slide as $row) {
						if($i == 0) { $class = "active"; } else { $class = "";}?>
								<div class="item <?=$class?>">
								<?if(strpos($row,'/') !== false) {
									 $video = explode('/',$row)[3];?>
									 <div class="video-container">
										<iframe width="100%" src="https://www.youtube.com/embed/<?=$video?>?ps=blogger&controls=0&iv_load_policy=3" frameborder="0" allowfullscreen="true"></iframe>
									 </div>
								</div>
							 <?} else {?>
									 <img src="/assets/uploads/<?=$row?>" alt="banner_1">
								</div>
						<?}
							$i++;
					}?>
			</div>

			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</section>
	<!-- Get_Banner -->

	<!-- Product Detail -->
	<section id="product_detail">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="get_detail">
						<!--
						<div class="collabo">
							<div class="clx">
								<p>X</p>
							</div>
							<div class="col-xs-6">
								<div class="logo sym_1" style="background-image: url('/assets/uploads/<?=$data->product_logo?>');"></div>
								<p><?=$data->product_company?></p>
							</div>
							<div class="col-xs-6">
								<div class="logo sym_2" style="background-image: url('/assets/uploads/<?=$data->insure_logo?>');"></div>
								<p><?=$data->insure_company?></p>
							</div>
						</div>
						-->
						<div class="product_title">
							<h2><?=$data->product_name?></h2>
						</div>
						<div class="hashtag">
							<?=$data->product_hashtag?>
						</div>
						<div class="more-btn">
							<button onclick="location.href='/main/call/<?=$data->product_idx?>'" data-idx = "<?=$data->product_idx?>">
								<i class="far fa-shopping-cart"></i> GET
							</button>
						</div>
						<a href="/main/about" class="about_link">이용방법 살펴보기</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Product Detail_END -->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Javascript files -->
	<script src="/assets/lib/waypoints/waypoints.min.js"></script>
	<script src="/assets/lib/jquery.counterup.min.js"></script>

	<script src="/assets/js/global.js"></script>
	<script>
	$("#myCarousel").carousel('pause');
	</script>
</body>
</html>
