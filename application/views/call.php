<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/call.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/main/index">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Call_Topfix -->
	<div id="call_topfix">
		<div class="call_area">
			<div class="thumb">
				<?$image = explode(',',$data->product_slide);?>
				<img src="/assets/uploads/<?=$image[0]?>" alt="">
			</div>
			<div class="cont">
				<p>현재 선택하신 사은품입니다.</p>
				<p><?=$data->product_name?></p>
			</div>
			<div class="call-btn col-xs-12">
				<p>
					<i class="fad fa-chevron-double-down animation-down"></i>
					광고영상보고 Call하면 Get완료
					<i class="fad fa-chevron-double-down animation-down"></i>
				</p>
				<button id="getcall" onclick="location.href='tel:<?=$data->insure_call?>'" data-idx = "<?=$data->product_idx?>"><i class="fas fa-phone-alt"></i><?=$data->insure_call?></button>
			</div>
		</div>
	</div>
	<!-- Call_Topfix_END -->

	<!-- Product_Insure -->
	<section id="product_insure">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="get_detail">
						<div class="collabo">
							<div class="col-xs-12">
								<div class="logo sym_1" style="background-image: url('/assets/uploads/<?=$data->insure_logo?>');"></div>
								<p><?=$data->insure_company?></p>
							</div>
						</div>
						<div class="product_title">
							<h2><?=$data->insure_name?></h2>
						</div>
						<div class="video-container">
							<?echo $video = explode('/',$data->insure_video)[3];?>
							<iframe width="100%" src="https://www.youtube.com/embed/<?=$video?>?ps=blogger&controls=0&iv_load_policy=3" frameborder="0" allowfullscreen="true"></iframe>
						</div>
						<div class="call-btn col-xs-12">
							<p>
								<i class="fad fa-chevron-double-down animation-down"></i>
								광고영상보고 Call하면 Get완료
								<i class="fad fa-chevron-double-down animation-down"></i>
							</p>
							<button id="getcall" onclick="location.href='tel:<?=$data->insure_call?>'" data-idx = "<?=$data->product_idx?>"><i class="fas fa-phone-alt"></i><?=$data->insure_call?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Product Insure_END -->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Javascript files -->
	<script src="/assets/lib/waypoints/waypoints.min.js"></script>
	<script src="/assets/lib/jquery.counterup.min.js"></script>

	<script src="/assets/js/common.js"></script>
	<script src="/assets/js/callgetdev.js"></script>
	<script src="/assets/js/global.js"></script>
</body>
</html>
