<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Main_Banner -->
	<section id="main_banner" class="main_banner mt50">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?$i=0; foreach($slide as $row) {
					if($i == 0){ $class = "active"; } else { $class = "";}?>
					<div class="item <?=$class?>">
						<img src="/assets/uploads/<?=$row->slide_image?>" alt="<?=$row->slide_name?>" data-link="<?=$row->slide_link?>">
					</div>
				<?$i++;}?>

			</div>
		</div>
	</section>
	<!-- Main_Banner_END -->

	<!-- Product -->
	<section id="product">
		<div class="container">
			<div class="row">
				<div class="section-header">
					<h3>CALL GET FOR YOU</h3>
				</div>
			</div>
			<div class="row">

			<?foreach($product as $row) {
				$image = explode(',',$row->product_slide)[0];?>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="product_area" onclick="location.href='/main/get/<?=$row->product_idx?>'">
						<div class="bg p_1" style=" background-image: url('/assets/uploads/<?=$image?>');">
							<div class="get_count">
								<div>현재까지<br>Get한 수량</div>
								<div><b><?=$row->product_get?></b>개</div>
							</div>
							<div class="count gray">
								<p>남은 기간</p>
								<p class="mb10"><?=$row->dates?><span>일</span></p>
							</div>
							<div class="contents">
								<div class="cont-area">
									<div class="brand">
										<div class="logo p_1 sym_1" style="background-image: url('/assets/uploads/<?=$row->product_logo?>');"></div>
										<div class="logo p_1 sym_2" style="background-image: url('/assets/uploads/<?=$row->insure_logo?>');"></div>
										<div class="title">
											<p><?=$row->product_company?> X <?=$row->insure_company?></p>
											<p><?=$row->product_name?></p>
										</div>
									</div>
									<hr>
									<div class="hashtag">
										<?=$row->product_hashtag?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?}?>

				<div class="col-xs-12">
					<div class="more-btn" data-count = "<?=$count?>" data-now = "1">
						<button>더보기</button>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Product_END -->

	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 p0">
					<div class="foot_cont">
						상호명 : (주)에스비제이 | 대표 : 정일웅 | 주소 : 서울시 강남구 테헤란로 116, 1064호 | 전화 : 02-544-1117 | 메일 : ad@newturntree.com | 사업자등록번호 : 894-86-01505
					</div>
				</div>
				<div class="col-xs-12 copyright">
					<p>Copyright &copy; Callget. All rights reserved.</p>
				</div>
			</div><!-- row end -->
		</div><!-- Container_END -->
	</footer>
	<!-- Footer_END -->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Javascript files -->
	<script src="/assets/js/common.js"></script>
	<script src="/assets/js/callgetdev.js"></script>
	<script src="/assets/js/global.js"></script>

</body>
</html>
