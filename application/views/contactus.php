<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />

	<meta name="keywords" content="Callget">
	<meta name="description" content="Callget">

	<link href="/assets/images/favicon/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="/assets/images/favicon/favicon.png" rel="icon" type="image/x-icon">

	<title>CALLGET</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/lib/animate/animate.css" rel="stylesheet" type="text/css">

	<link href="/assets/css/global.css" rel="stylesheet" type="text/css">
	<link href="/assets/css/contactus.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status"></div>
	</div>
	<!-- Preloader_END -->

	<!-- Navigation -->
	<header>
		<nav class="navbar navbar-global navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="logo">
					<a href="/main/index">
						<img src="/assets/images/logo.png" />
					</a>
				</div>

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="collapse navbar-collapse" id="custom-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/main/about">콜겟이란?</a>
						</li>
						<li>
							<a href="/main/faq">이용방법</a>
						</li>
						<li>
							<a href="/main/contactus">문의사항</a>
						</li>
					</ul>
				</div>
			</div><!-- Container_END -->
		</nav>
	</header>
	<!-- Navigation_END -->

	<!-- Contact US -->
	<section id="contactus" class="mt50">
		<div class="container">
			<div class="section-header">
				<h3>Contact Us</h3>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="tab-menu" role="tabpanel" data-example-id="togglable-tabs">
						<div class="tab_head">
							<ul id="myTab" class="nav nav-tabs bar_tabs menu_tabs" role="tablist">
								<li role="presentation" class="active">
									<button data-target="#tab_content1" role="tab" data-toggle="tab" aria-expanded="true">이용문의</button>
								</li>
								<li role="presentation" class="">
									<button data-target="#tab_content2" role="tab" data-toggle="tab" aria-expanded="false">제휴문의</button>
								</li>
							</ul>
						</div>

						<div id="myTabContent" class="tab-content">
							<div id="tab_content1" class="tab-pane fade active in" role="tabpanel" aria-labelledby="이용문의">
								<div class="form">
									<div id="sendmessage">메세지 전송이 완료되었습니다</div>
									<div id="errormessage"></div>
									<form action="" class="contactForm" method = "post">
										<div class="form-row">
											<div class="form-group col-md-6">
												<input type="text" id="formname" class="form-control" placeholder="성함" data-msg="성함이 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group col-md-6">
												<input type="email" id="formemail" class="form-control" placeholder="이메일" data-msg="이메일이 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group form-padding">
												<textarea id="formtext" class="form-control" rows="5" placeholder="문의사항" data-msg="문의사항이 입력되지 않았습니다." required="required"></textarea>
												<div class="validation"></div>
											</div>
											<div class="text-center">
												<button type="submit" id="faq">이용문의 발송</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div id="tab_content2" class="tab-pane fade" role="tabpanel" aria-labelledby="제휴문의">
								<div class="form">
									<div id="sendmessage_">메세지 전송이 완료되었습니다</div>
									<div id="errormessage"></div>
									<form role="form" class="contactForm_" onsubmit="return false;">
										<div class="form-row">
											<div class="form-group col-xs-12 col-sm-6">
												<input type="text" id="name_" class="form-control" placeholder="성함" data-msg="성함이 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group col-xs-12 col-sm-6">
												<input type="text" id="phone_" class="form-control" placeholder="연락처" data-msg="연락처가 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group col-xs-12 col-sm-6">
												<input type="text" id="company_" class="form-control" placeholder="회사명" data-msg="회사명이 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group col-xs-12 col-sm-6">
												<input type="text" id="manager_" class="form-control" placeholder="부서/직급" data-msg="부서/직급이 입력되지 않았습니다." required="requried"/>
												<div class="validation"></div>
											</div>
											<div class="form-group col-xs-12">
												<input type="email" id="email_" class="form-control" placeholder="이메일" data-msg="이메일이 입력되지 않았습니다." required="required"/>
												<div class="validation"></div>
											</div>
											<div class="form-group form-padding">
												<textarea class="form-control" id="text_" rows="5" placeholder="제휴문의" data-msg="제휴문의가 입력되지 않았습니다." required="required"></textarea>
												<div class="validation"></div>
											</div>
											<div class="text-center">
												<button type="submit">제휴문의 발송</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Contact US_END -->

	<!-- Location -->
	<section id="location" class="wow fadeInUp">
		<div class="container">
			<div class="section-header mb30">
				<h3>Location</h3>
				<p>
					Tel. 02-555-1114<br>
					E-mail. ad@newturntree.com
				</p>
			</div>
			<div class="row">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3164.2191271409833!2d127.02805661558749!3d37.52633153418668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca38378602e83%3A0x27e8ef57be0c8a6e!2z64m07YS07Yq466as7JWg65Oc!5e0!3m2!1sko!2skr!4v1571890453412!5m2!1sko!2skr" width="100%" height="200px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</section>
	<!-- Location_END -->

	<!-- Footer -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 p0">
					<div class="foot_cont">
						상호명 : (주)에스비제이 | 대표 : 정일웅 | 주소 : 서울시 강남구 테헤란로 116, 1064호 | 전화 : 02-544-1117 | 메일 : ad@newturntree.com | 사업자등록번호 : 894-86-01505
					</div>
				</div>
				<div class="col-xs-12 copyright">
					<p>Copyright &copy; Callget. All rights reserved.</p>
				</div>
			</div><!-- row end -->
		</div><!-- Container_END -->
	</footer>
	<!-- Footer_END -->

	<!-- Scroll to top -->
	<div class="scroll-up" title="TOP으로 이동" alt="TOP으로 이동">
		<a href="#home"><span class="glyphicon glyphicon-menu-up"></span></a>
	</div>
	<!-- Scroll to top_END-->

	<!-- Contact Form JavaScript File -->
	<script src="/assets/js/common.js"></script>
	<script src="/assets/contactform/contactform.js"></script>

	<!-- Javascript files -->
	<script src="/assets/js/global.js"></script>
</body>
</html>
