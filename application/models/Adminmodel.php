<?php
class adminmodel extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
    }
    // 로그인
    public function insert_login() {
        // $_POST['id'] = 'callget';
        // $_POST['pass'] = 'admin';
        foreach($_POST as $key => $value ){
            $key = 'admin_'.$key;
            if($key == "admin_pass") {
                $value = password_hash($value , PASSWORD_DEFAULT, ['cost'=>12]);
            }
            $array[$key] = $value;
        }
        $result = $this->db->insert('callget_admin', $array);
        return array('return'=>$result);
    }

    public function loginData() {
      // $_POST['id'] = 'callget';
      // $_POST['pass'] = 'admin';
      $sql="SELECT admin_idx, admin_id, admin_pass FROM callget_admin WHERE admin_id = ?";
      $array = array($_POST['id']);
      $data = $this->db->query($sql, $array)->row();

      if (password_verify($_POST['pass'] ,$data->admin_pass) ) {
          $_SESSION['id'] = $_POST['id'];
          $_SESSION['idx'] = $data->admin_idx;
          return array('return'=>true);
      }
      else {
          return array('return'=>false);
      }
    }

    public function slideListData($page = 1) {
        $table = "callget_slide";
        $limit=10;
        $offset=$limit*($page-1);
        $sql="SELECT slide_idx, @rownum:=@rownum+1 num, slide_name, slide_image,
                      slide_link, slide_date
                FROM callget_slide, (SELECT @rownum:=0) TMP
                ORDER BY slide_idx DESC";
              // limit {$limit} offset {$offset}
        $result = $this->db->query($sql)->result();

        $sql="SELECT COUNT(*) count FROM $table";
        $count = $this->db->query($sql)->row();

        return array('return'=>true,'list'=>$result,'count'=>$count->count);
    }

    public function slideinsertData() {
        $table = "callget_slide";
        foreach($_POST as $key => $value ){
            $key = 'slide_'.$key;
            $array[$key] = $value;
        }
        $array['slide_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($table, $array);
        return array('return'=>$result);
    }

    public function slideData($idx) {
      $array = array($idx);
      $sql="SELECT slide_idx, slide_name, slide_image, slide_link
            FROM callget_slide WHERE slide_idx = ?";
            return $this->db->query($sql, $array)->row();
    }

    public function slideUpdate() {
      $array = array($_POST['name'], $_POST['image'], $_POST['link'], $_POST['idx']);
      $sql= "UPDATE callget_slide SET slide_name = ?, slide_image = ?,
                slide_link = ? WHERE slide_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }

    public function slidedel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM callget_slide WHERE slide_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }
    //카테고리
    public function categoryListData($page = 1) {
        $table = "callget_category";
        $limit=10;
        $offset=$limit*($page-1);
        $sql="SELECT category_idx, @rownum:=@rownum+1 num, category_name
                  , category_image, category_date
                FROM callget_category, (SELECT @rownum:=0) TMP
                ORDER BY category_idx DESC ";
              // limit {$limit} offset {$offset}
        $result = $this->db->query($sql)->result();

        $sql="SELECT COUNT(*) count FROM $table";
        $count = $this->db->query($sql)->row();

        return array('return'=>true,'list'=>$result,'count'=>$count->count);
    }

    public function categoryinsertData() {
        $table = "callget_category";
        foreach($_POST as $key => $value ){
            $key = 'category_'.$key;
            $array[$key] = $value;
        }
        $array['category_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($table, $array);
        return array('return'=>$result);
    }

    public function categoryData($idx) {
      $array = array($idx);
      $sql="SELECT category_idx, category_name, category_image
            FROM callget_category WHERE category_idx = ?";
            return $this->db->query($sql, $array)->row();
    }

    public function categoryUpdate() {
      $array = array($_POST['name'], $_POST['image'], $_POST['idx']);
      $sql= "UPDATE callget_category SET category_name = ?, category_image = ?
                WHERE category_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }

    public function categorydel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM callget_category WHERE category_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }

    //faq
    public function faqinsertData() {
        $table = "callget_faq";
        foreach($_POST as $key => $value ){
            $key = 'faq_'.$key;
            $array[$key] = $value;
        }
        $array['faq_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($table, $array);
        return array('return'=>$result);
    }

    public function faqListData($page = 1) {
        $table = "callget_faq";
        $limit=10;
        $offset=$limit*($page-1);
        $sql="SELECT faq_idx, @rownum:=@rownum+1 num, faq_question, faq_answer
                FROM callget_faq, (SELECT @rownum:=0) TMP
                ORDER BY faq_idx DESC";
              // limit {$limit} offset {$offset}
        $result = $this->db->query($sql)->result();

        $sql="SELECT COUNT(*) count FROM $table";
        $count = $this->db->query($sql)->row();

        return array('return'=>true,'list'=>$result,'count'=>$count->count);
    }

    public function faqData($idx) {
      $array = array($idx);
      $sql="SELECT faq_idx, faq_question, faq_answer
            FROM callget_faq WHERE faq_idx = ?";
            return $this->db->query($sql, $array)->row();
    }

    public function faqUpdate() {
      $array = array($_POST['question'], $_POST['answer'], $_POST['idx']);
      $sql= "UPDATE callget_faq SET faq_question = ?, faq_answer = ?
                WHERE faq_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }

    public function faqdel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM callget_faq WHERE faq_idx = ?";
      $result = $this->db->query($sql, $array);
      return array('return'=>$result);
    }

    //상품 관리
    public function productinsertData() {
        $producttable = "callget_product";
        $insuretable = "callget_insure";

        $product['category_idx'] = $_POST['category_idx'];

        foreach($_POST as $key => $value ){
          if($value != "") {
            if(strpos($key,'product') !== false) {
              $product[$key] = $value;
            }
            else {
              if($key != "category_idx")
                $insure[$key] = $value;
            }
          }
        }
        $product['product_date'] = date('Y-m-d H:i:s');
        $result = $this->db->insert($producttable, $product);

        $insert_id = $this->db->insert_id();

        $insure['product_idx'] = $insert_id;
        $result_ = $this->db->insert($insuretable, $insure);
        if($result && $result_) {
          return array('return'=>true);
        }
        else {
          return array('return'=>false);
        }
    }

    public function categorySelectData() {
      $sql="SELECT category_idx, category_name FROM callget_category";
        return $this->db->query($sql, $array)->result();
    }

    public function productListData($page = 1) {
        $limit=10;
        $offset=$limit*($page-1);
        $sql="SELECT product_idx, @rownum:=@rownum+1 num, category_name, insure_name, insure_call,
                insure_company,insure_logo, insure_video, product_company,
                product_logo, product_slide, product_name, product_hashtag,
                CONCAT(TO_DAYS(CAST(product_time AS DATE)) -
                        TO_DAYS(CAST(now() AS DATE)),'일') AS dates
                ,product_get, product_date FROM callget_product
                JOIN callget_insure USING(product_idx)
                LEFT JOIN callget_category USING(category_idx)
                , (SELECT @rownum:=0) TMP
                ORDER BY product_idx DESC";
              // limit {$limit} offset {$offset}
        $result = $this->db->query($sql)->result();

        $sql="SELECT COUNT(*) count FROM callget_product
        JOIN callget_insure USING(product_idx) JOIN callget_category
        USING(category_idx)";
        $count = $this->db->query($sql)->row();

        return array('return'=>true,'list'=>$result,'count'=>$count->count);
    }

    public function productData($idx) {
      $sql="SELECT product_idx, category_idx, insure_name, insure_call,
              insure_company, insure_logo, insure_video, product_company,
              product_logo, product_slide, product_name, product_hashtag,
              product_time, product_slidevideo
              FROM callget_product JOIN callget_insure
              USING(product_idx) WHERE product_idx = ?";
      $array = array($idx);
      return $this->db->query($sql, $array)->row();
    }

    public function productUpdateData() {
        $producttable = "callget_product";
        $insuretable = "callget_insure";
        foreach($_POST as $key => $value ){
          if(strpos($key,'product') !== false || strpos($key,'category') !== false) {
            $product[$key] = $value;
          }
          else {
            $insure[$key] = $value;
          }
        }

        $this->db->where('product_idx', $_POST['product_idx']);
        $result = $this->db->update($producttable, $product);

        $this->db->where('product_idx', $_POST['product_idx']);
        $result_ = $this->db->update($insuretable, $insure);
        if($result && $result_) {
          return array('return'=>true);
        }
        else {
          return array('return'=>false);
        }
    }

    public function productdel($idx) {
      $array = array($idx);
      $sql= "DELETE FROM callget_product WHERE product_idx = ?";
      $result = $this->db->query($sql, $array);
      $sql= "DELETE FROM callget_insure WHERE product_idx = ?";
      $result_ = $this->db->query($sql, $array);
      if($result && $result_) {
        return array('return'=>true);
      }
      else {
        return array('return'=>false);
      }
    }

}
?>
