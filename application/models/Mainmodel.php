<?php
class mainmodel extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public function SlideData() {   //메인 슬라이드
        $sql="SELECT slide_idx, slide_image, slide_link, slide_name
         FROM callget_slide";
        $list = $this->db->query($sql, $array)->result();
        return array('return'=>true,'list'=>$list);
    }

    public function categoryData() {   //메인 카테고리
        $sql="SELECT category_idx, category_name, category_image
                FROM callget_category";
        $list = $this->db->query($sql, $array)->result();
        return array('return'=>true,'list'=>$list);
    }

    public function mainList($page = 1, $idx = null) {  //메인 리스트
        $limit=6;
        $offset=$limit*($page-1);
        if($idx) {
          $where = " AND category_idx = ".$idx;
        }
        $sql="SELECT product_idx, TO_DAYS(CAST(product_time AS DATE)) -
                TO_DAYS(CAST(now() AS DATE)) AS dates,
                product_get, product_logo, insure_logo,
                product_company, insure_company, product_name, product_slide,
                product_hashtag FROM callget_product
                JOIN callget_insure USING(product_idx)
                WHERE CURDATE() <= product_time ".$where."
                ORDER BY product_idx DESC
                limit {$limit} offset {$offset} ";
        $list = $this->db->query($sql)->result();

        $sql="SELECT COUNT(*) count FROM callget_product
                JOIN callget_insure USING(product_idx)
                WHERE CURDATE() <= product_time".$where;
        $count = $this->db->query($sql)->row()->count;
        return array('return'=>true,'list'=>$list,'count'=>ceil($count/6));
    }
    ///////////////////// 겟 페이지

    public function  getData($idx) {   //겟 데이터
        $sql="SELECT product_idx, CONCAT_WS(',',IF(product_slidevideo='',null,product_slidevideo),product_slide) product_slide,
                product_company, insure_company, product_logo, insure_logo, product_name,
                product_get, TO_DAYS(CAST(product_time AS DATE)) -
                TO_DAYS(CAST(now() AS DATE)) AS dates, product_hashtag
                 FROM callget_product JOIN callget_insure
                USING(product_idx) WHERE product_idx = ?";
        $array= array($idx);
        $data = $this->db->query($sql, $array)->row();
        return array('return'=>true,'data'=>$data);
    }
    /////////////콜 페이지
    public function  callData($idx) {
        $sql="SELECT product_idx, product_slide, product_name, insure_call,
                insure_logo, insure_company, insure_name, insure_video,
                TO_DAYS(CAST(product_time AS DATE)) -
                TO_DAYS(CAST(now() AS DATE)) AS dates,
                product_get FROM callget_product JOIN callget_insure
                USING(product_idx) WHERE product_idx = ?";
        $array= array($idx);
        $data = $this->db->query($sql, $array)->row();
        return array('return'=>true,'data'=>$data);
    }
    //////// faq 페이지
    public function  faqData() {
        $sql="SELECT faq_idx, faq_question, faq_answer FROM callget_faq";
        $list = $this->db->query($sql)->result();
        return array('return'=>true,'list'=>$list);
    }
    public function  updateGet($idx) {
        $array = array($idx);
        $sql="SELECT product_get FROM callget_product WHERE product_idx = ?";
        $count = $this->db->query($sql, $array)->row()->product_get;
        $count += 1;
        $array = array($count, $idx);
        $sql="UPDATE callget_product SET product_get = ? WHERE product_idx = ?";
        $result = $this->db->query($sql,$array);
        if($result){
          return array('return'=>true);
        }
    }


}
?>
