$(document).ready(function(){
     function admin() {
         this.slide = function() {
             $('body').on('click', '#slidego, #slideedit', function() {
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     if($(this).attr('id') == "slidego"){
                         var json = jsonreturn('/admin/slideinsertData',data);
                         if(json.return == true) {
                             alert("등록되었습니다");
                             location.href = "/admin/slideList";
                         }
                     }
                     else {
                         var json = jsonreturn('/admin/slideUpdate',data);
                         if(json.return == true) {
                             alert("수정되었습니다");
                             location.reload();
                         }
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });

             $('body').on('change', '#slideimage', function() {
                 var json = uploadImage($(this));
                 $(this).next().val(json.url[0]);
                 $(this).next().next().css('display','block');
                 $(this).next().next().attr('src','http://callget.co.kr/assets/uploads/'+json.url[0]);
             });
         }
         this.category = function() {
             $('body').on('click', '#categorygo, #categoryedit', function() {
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     if($(this).attr('id') == "categorygo"){
                         var json = jsonreturn('/admin/categoryinsertData',data);
                         if(json.return == true) {
                             alert("등록되었습니다");
                             location.href = "/admin/categoryList";
                         }
                     }
                     else {
                         var json = jsonreturn('/admin/categoryUpdate',data);
                         if(json.return == true) {
                             alert("수정되었습니다");
                             location.reload();
                         }
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });
         }
         this.faq = function() {
             $('body').on('click', '#faqgo, #faqedit', function() {
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     if($(this).attr('id') == "faqgo"){
                         var json = jsonreturn('/admin/faqinsertData',data);
                         if(json.return == true) {
                             alert("등록되었습니다");
                             location.href = "/admin/faqList";
                         }
                     }
                     else {
                         var json = jsonreturn('/admin/faqUpdate',data);
                         if(json.return == true) {
                             alert("수정되었습니다");
                             location.reload();
                         }
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });
         }
         this.product = function() {
             $('body').on('click', '#productgo, #productedit', function() {
                 var check = true;
                 $('.req').each(function() {
                     if (!$(this).val()) {
                          check = false;
                          return false;
                     }
                 });
                 if(check) {
                     var form = $('form')[0];
                     var data = new FormData(form);
                     if($(this).attr('id') == "productgo"){
                         var json = jsonreturn('/admin/productinsertData',data);
                         if(json.return == true) {
                             alert("등록되었습니다");
                             // location.href = "/admin/productList";
                         }
                     }
                     else {
                         var json = jsonreturn('/admin/productUpdateData',data);
                         if(json.return == true) {
                             alert("수정되었습니다");
                             location.reload();
                         }
                     }
                 }
                 else {
                     $(this).next().trigger('click');
                 }
             });
         }
         this.login = function() {
           $('body').on('click', '#logingo', function() {
               var req = true;
               $('.req').each(function() {
                   if (!$(this).val()) {
                       req = false;
                       return false;
                   }
                });

               if(req) {
                   var form = $('form')[0];
                   var data = new FormData(form);
                   var json = jsonreturn('/admin/loginData',data);
                   if(json.return == true) {
                       location.href = '/admin/slideList';
                   }
                   else {
                       alert("아이디 비밀번호가 일치하지 않습니다");
                   }
               }
               else {
                   $(this).next().trigger('click');
               }
           });

           $(".log").keypress(function(e) {
            if (e.keyCode == 13){
              var req = true;
              $('.req').each(function() {
                  if (!$(this).val()) {
                      req = false;
                      return false;
                  }
               });

              if(req) {
                  var form = $('form')[0];
                  var data = new FormData(form);
                  var json = jsonreturn('/admin/loginData',data);
                  if(json.return == true) {
                      // alert("로그인 되었습니다");
                      location.href = '/admin/slideList';
                  }
                  else {
                      alert("아이디 비밀번호가 일치하지 않습니다");
                  }
              }
              else {
                  $(this).next().trigger('click');
              }
            }
          });


         }

         $('body').on('change', '#logo, #logos, #slide', function() {
            var json = uploadImage($(this));
            if (json) {
              var url = json.url.join();
              $(this).next().val(url);
              if(json.url[1]) {
                $('.slidess').remove();
                for(i = 0; i < json.url.length; i++){
                  $('.youtubb').after('<img src="http://callget.co.kr/assets/uploads/'+json.url[i]+'" class = "slidess">');
                  $(this).next().next().css('display','block');
                }
              }
              else {
                $(this).next().next().attr('src','http://callget.co.kr/assets/uploads/'+json.url[0]);
                $(this).next().next().css('display','block');
              }
            }
          });
          // $('body').on('click', '.adminview', function() {
          //     var idx = $(this).parent().parent().attr('data-idx');
          //     location.href = '/admin/estimateView/'+idx;
          // });

    }
    var admin = new admin();
    admin.slide();
    admin.category();
    admin.faq();
    admin.product();
    admin.login();

});
